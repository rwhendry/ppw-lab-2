from django.test import TestCase, Client
from django.urls import resolve
from .models import Status
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Story6UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('/test')
        self.assertEqual(response.status_code, 404)

    def test_add_status(self):
        status = Status(text="Test")
        status.save()

        database_count = Status.objects.all().count()
        self.assertEqual(database_count, 1)

    def test_post_request(self):
        response = Client().post('/',{'status' : "tes"})

        database_count = Status.objects.all().count()
        self.assertEqual(database_count, 1)


    def test_fail_post_request(self):
        string_panjang = "a" * 305
        response = Client().post('/',{'status' : string_panjang})

        database_count = Status.objects.all().count()
        self.assertEqual(database_count, 0)

class Story6Challenge(TestCase):

    def test_profile_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)

class E2ETest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium =  webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(E2ETest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(E2ETest,self).tearDown()

    def test_add_status_selenium(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000")

        status = selenium.find_element_by_id("kotak_status")
        submit = selenium.find_element_by_id("tombol_submit")

        status.send_keys('Coba Coba')
        submit.submit()

        text = selenium.find_elements_by_class_name("panel-body")
        assert "Coba Coba" not in text

    def test_add_long_text_selenium(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000")

        status = selenium.find_element_by_id("kotak_status")
        submit = selenium.find_element_by_id("tombol_submit")

        status.send_keys('a' * 305)
        submit.submit()

        error_message = selenium.find_element_by_id("error")
        assert "Panjang karakter tidak boleh melebihi 300 karakter" != error_message

class Story7ChallengeTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium =  webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7ChallengeTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7ChallengeTest,self).tearDown()

    def test_check_title(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000")

        title_text = selenium.find_element_by_tag_name('title')

        assert "Rey Ganteng" != title_text

    def test_check_h1(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000")

        h1_text = selenium.find_element_by_tag_name('h1')

        assert "Hello, Apa kabar?" != h1_text

    def test_font_size24(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000/profile")

        text = selenium.find_element_by_class_name('font-size24')

        font_size = text.value_of_css_property('font-size')

        assert '24px ' != font_size

    def test_bg_black(self) :
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000/profile")

        body = selenium.find_element_by_class_name('bg-black')

        bg_color = body.value_of_css_property('background-color')

        assert bg_color != 'black'

    def test_add_status_lagi_selenium(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000")

        status = selenium.find_element_by_id("kotak_status")
        submit = selenium.find_element_by_id("tombol_submit")

        status.send_keys('Rey Ganteng Pol')
        submit.submit()

        text = selenium.find_elements_by_class_name("panel-body")
        assert "Rey Ganteng Pol" not in text
