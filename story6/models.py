from django.db import models
from django.utils import timezone

# Create your models here.

class Status(models.Model):
    text = models.CharField(max_length = 300)
    time = models.DateTimeField(default = timezone.now)
