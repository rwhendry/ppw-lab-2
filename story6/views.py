from django.shortcuts import render
from .models import Status

# Create your views here.

def index(request):
    if request.method == 'POST':
        try:
            text = request.POST.get('status')
            status = Status()
            status.text = text
            status.clean
            status.full_clean()
            status.save()
        except:
            list_status = Status.objects.all()
            list_status = reversed(list_status)
            context = {"list_status" : list_status, "error" : "Panjang karakter tidak boleh melebihi 300 karakter"}
            return render(request, 'index.html', context)

    list_status = Status.objects.all()
    list_status = reversed(list_status)
    context = {"list_status" : list_status}
    return render(request, 'index.html', context)

def profile(request):
    return render(request, 'profile.html')
