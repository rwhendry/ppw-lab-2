
function get_book(){

  key = $("#val").val()
  let url = ""
  if(key == "") {
    url = '/book'
  }
  else url = '/book?search='+key
  console.log(url)
  $.ajax(url,
        {
          success: function(result){
            const items = result["data"]["items"];
            const fav = result["fav"]
            let title = '';
            let author = '';
            let favourite = '';
            let id = 0;
            for (let i = 0; i < items.length; i++) {
              title += `<div class="mar-top-20">` +  items[i]["volumeInfo"]["title"] + "</div>"
              auth = ""
              if((items[i]["volumeInfo"]["authors"])) auth = auth + items[i]["volumeInfo"]["authors"][0];
              author += `<div class="mar-top-20">` + auth + "</div>"
              let fa = ""
              if(String(fav[items[i]["id"]]) == "false") {
                fa = "favourite"
              }
              else fa = "unfavourite"
              favourite += `<button class ="mar-top-15" id="` + items[i]["id"] + `" onclick=add_favourite('` + items[i]["id"]  +`')> `+ fa + ` </button> <br>`
              id++;
            }
            title = `<div class="col-xs-6"> <h4> Title </h4> ` + title  + `</div>`
            author = `<div class="col-xs-4"> <h4> Author </h4>` + author + `</div>`
            favourite = `<div class="col-xs-2"> <h4> Favourite </h4>` + favourite + `</div>`

            $("#favourite").html(title + author + favourite);
          }
    });
}

cnt = 0;

function add_favourite(id){
  $.ajax(
    '/addfavourite',
    {
      data: {
        book_id: id,
      },
      success: function(result){
          console.log(result["favourite"])
          if(String(result["favourite"]) == "false") {
           $("#"+id).html("favourite");
         }
         else {
           $("#"+id).html("unfavourite");         }
        $("#total").html(result["count"])
      }
    }
  )


}
