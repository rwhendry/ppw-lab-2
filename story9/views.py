from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import logout
import requests


def render_book(request):
    search = request.GET.get('search', "quilting")
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + search

    unfavourite = {}
    data = requests.get(url).json()

    fav = {}
    for key in data["items"]:
        #print(key["id"])
        if request.session.get(key["id"], False):
            fav[key["id"]] = True
        else:
            fav[key["id"]] = False

    return JsonResponse({'data' : data, "fav" : fav})

def book(request):
    if(not request.user.is_authenticated):
        return render(request, 'book.html')
    else:
        name = request.user.first_name + " " + request.user.last_name
        cnt = request.session.get('favorite_count', 0)
        #print(cnt)
        return render(request, 'book.html', {'name' : name, 'count' : cnt})

def logoutt(request):
    logout(request)
    return redirect('/story9')

def add_favourite(request):
    cnt = request.session.get('favorite_count', 0)
    #print("a")
    data = request.session.get(request.GET.get("book_id"), False)
    fav = True
    if(not data):
        cnt += 1
        request.session[request.GET.get("book_id")] = True
        fav = True
    else:
        cnt -= 1
        request.session[request.GET.get("book_id")] = False
        fav = False

    request.session['favorite_count'] = cnt
    #print(request.session.get('favorite_count', 0))
    return JsonResponse({'count': cnt, 'favourite' : fav})
